      @<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Paulo's Personal Webpage - Educational Reforms of Tsar Peter I</title>
        <link rel="stylesheet" href="../css/default.css" />
        <link rel="icon" href="../images/favicon.png" />
        <body link="#BB5A5A" vlink="#BB5A5A" alink="#9A7878">
        <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
        <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js">
        </script>
    </head>
    <body>
        <header>
            <div class="logo">
                <a href="../">Paulo's Personal Webpage</a>
            </div>
            <nav>
                <a href="../">Home</a>
                <a href="../contact.html">Contact</a>
                <a href="../archive.html">Archive</a>
                <a href="../writings.html">Writings</a>
                <a href="../resources.html">Resources</a>
            </nav>
        </header>

        <main role="main">
            <h1>Educational Reforms of Tsar Peter I</h1>
            <article>
    <section class="header">
        Posted on 2022-07-27
        
    </section>
    <section class="header">
        
        Tags: <a title="All pages tagged 'History'." href="../tags/History.html">History</a>, <a title="All pages tagged 'Russia'." href="../tags/Russia.html">Russia</a>
        
    </section>
    <section>
        <h1 id="table-of-contents">Table of Contents</h1>
<ul>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#peter-i-through-historical-lenses">Peter I through historical lenses</a></li>
<li><a href="#youth">Youth</a></li>
<li><a href="#contemporary-views">Contemporary views</a></li>
<li><a href="#his-educational-reforms">His Educational reforms</a></li>
<li><a href="#religious-education">Religious Education</a></li>
<li><a href="#higher-education">Higher Education</a></li>
<li><a href="#references">References</a></li>
</ul>
<h1 id="introduction">Introduction</h1>
<p>Peter’s Major reforms, was a constructive “revolution” in the area of education. He aimed to transform and modernize his nation “for the better”, this process of modernization was based mainly upon the westernization, by bringing elements of Western Europe into Russia.</p>
<p><img src="../images/peter-peter.jpg" style="float: right" width="200" /></p>
<p>His Reign was characterized by wars with the Kingdom of Sweden and the Ottomans, conflicts that originated prior to his ascension to the throne. Peter I, by the introduction of a new bureaucratic model, created a proper navy for Russia and reformed the military. Peter was adherent to the Idea of a ‘Universal Service State’. His reforms entailed many cultural and economic changes within Russia.</p>
<h1 id="peter-i-through-historical-lenses">Peter I through historical lenses</h1>
<p>Even centuries after his reign, Peter I still is a controversial figure, both among scholars and the general population. Regardless of political opinions, education or religious adherence, one can easily find divergent opinions on Peter I; there still are discussions on whether his reforms were beneficial or not to Russia, or even if they were radical at all, as Platonov discusses on his ‘Peter the Great Not a Revolutionary Innovator’ book: “The same class relations and the agricultural economy remained. The cultural ideals had already begun change prior to Peter, so Peter was simply the first Tsar who tool the time and initiative to implement reforms in that area. The illusion of a revolutionary, by destroying a old order and created a new one only happened due to this speedy reforms, contrasted with the cautious and slow policies of past Tsars, and the manner in which he went about reforming.”</p>
<p>Although, it seems most historians agree that Peter I made a substantial impact on Russia as a state and on Russian culture as well. As Sumner comments: “Modern Education in Russia, not confined to one class. Sending students abroad, creating the Holy Synod, formation of a navy and the moving of the capital.” (Peter’s Accomplishments and Their Historical Significance in ‘Peter the Great: Reformer or Revolutionary?’)</p>
<p>Nicholas V. Riasanovksy points out in his book ‘The Image of Peter the Great in Russian History and Thought (1985)’ that the image of Peter’s reign changes as a new period in Russian History develops, being it a negative of a positive image.</p>
<h1 id="youth">Youth</h1>
<p>The Russia Peter grew up in was closed to external influences, foreigners could only inhabit satellite settlements around Moscow. As a young boy, Peter would sneak into these settlements, mainly occupied by foreigners from Western Europe, there he could observe Western ways of living and get in contact to different cultures, it’s said that he learned basic concepts of geometry and geography there, clearly indicating a significant deficiency in the educational system in Russia. It seems that Peter got fascinated by the things he learned by visiting these settlements, early on in his reign he decided to travel through Europe. He was exposed to many cultural differences, the novel technological discoveries and inventions and the western philosophical and political ideals of a nation state. Inspired by his travels, Peter brought new ideas back to Russia with the aim to develop Russia and achieve “greatness” among Europe, Peter knew that he needed to implement significant, if not drastic reforms if Russia were to become a Great Power and compete within Europe.</p>
<h1 id="contemporary-views">Contemporary views</h1>
<p>Contemporaries of the Tsar attested the significant and productive changes implemented by him, as well as his curiosity and love of learning.</p>
<p><img src="../images/peter-duke.jpg" style="float: right" width="150" /></p>
<p>During his trip to France in 1717, Duke Rouvroy recalls that Peter is “much admired for his curiosity.” He was always interested in topics related with the government, commerce and education. According to Rouvroy, Peter’s curiosity “reached into everything and disdained nothing.” His character portrated wisdom and good sense and everything about him testified to his extraordinary intelligence, Rouvroy believes Peter to be worthy of praise and mentions that France considers him to be a prodigy. Rouvroy’s account is an example of a foreigner’s opinion of Peter the Great, a Western European aristocrat belonging to one of the most influential nations at the time.</p>
<p><img src="../images/peter-lomonosov.jpg" style="float: left" width="150" /></p>
<p>Lomonosov’s speech at the coronation of Peter’s daughter Elizabeth, expresses his praise of Peter and deems him as the “Father of the Country” as he estabilished a new navy, new towns, public and private buildings in the Western European style, founded a judiciary, senate, instituted government offices and state colleges. Peter also invited experienced scholars into Russia to spread the study of science and the arts. Lomonosov describes Peter’s trip to Europe as a way to observe what other countries had, which might then be utilized in Russia and benefit the country as a whole. He mentioned the educational foundations laid down by Peter and esteemed the Tsar’s intelligence.</p>
<p>These Personal qualities of Peter are an indication of his particular interest in advancing the educational system in Russia. He clearly understood the value of learning, Peter then tried to bring that “love of education” he had to Russia, which was not an easy task.</p>
<h1 id="his-educational-reforms">His educational reforms</h1>
<p>Peter’s reign had a substantial impact throughout Russia, he implemented reforms in many areas of society, from the peasant to the aristocrat. However, many people have different opinions regarding the approaches and the nature itself of said reforms.</p>
<p>Some decrees made by Peter:</p>
<ul>
<li>Initiation of the compulsory education for the nobility in 1714</li>
<li>The establishment of his ‘Spiritual Regulation’ of 1721</li>
<li>Founding of the Academy of Sciences in 1724.</li>
</ul>
<p>There was a increased focus on education and consequently a clear increase in the vocabulary of the Russian language, primarily due to influence of the classics, modern ideals and scientific terminology. French astronomers, as well as Dutch, Italian and British naval officers brought new technical knowledge that was, in turn taught to Russian students.</p>
<p>At the beginning of 1698, Peter kept a journal about his endeavors as the Tsar, there were some entries mentioning the changes he implemented within the country.</p>
<ul>
<li>In 1697, Peter began sending Russian students abroad to study navigation, in groups of 40 to 50, in places like the Netherlands, England, France and the Italian Peninsula.
<ul>
<li>The students were instructed to learn all they could about naval endeavors, such as how to navigate, use the compass and to draw plans.</li>
<li>After receiving a signature certifying their competence for naval duties, they would return to Russia and teach another officer upon their return.</li>
<li>Peter would offer monetary incentives to those willing to promote and spread the knowledge of the science of navigation then acquired.</li>
<li>Besides Naval Science, students were also sent abroad to study foreign languages, law, economics, medicine, arts and architecture.</li>
</ul></li>
<li>In 1699, there is an entry about social reforms, more specifically the advance of the printing press in Russia, as consequence there were translations and the printing of books about artillery, mechanics, history and other scholar pursuits. He also mentions the increase of the number of schools in Russia, the founding of a “school of marine” and schools concerned with “other arts and sciences.” He recalled that it used to be illegal for students to study sciences abroad, but that under his government, it’s not only allowed but mandatory to some.</li>
</ul>
<p>Peter’s involvement and concern about reforming the education system in Russia shows that Peter considered the development in education as necessary for the development of Russia as a Great Power. Building a Navy and taking education and technology as crucial for Russia’s strength.</p>
<ul>
<li>In 1714, it was issued compulsory education for the nobility. He mandated that the children of the nobility, as well as government clerks and officials, between the ages of ten and fifteen to learn arithmetic and geometry. After learning what was required, with salaries paid by the state, they would be sent to other parts of the country to teach, only after mastering the material and completing their assignments as teachers, they would receive a certificate of completion that was a requirement for marriage. They were not allowed to get married otherwise.</li>
</ul>
<p>The introduction of compulsory education for the Russian nobility changed the way education was viewed by the Russian population, before the decree, secular education was not considered to be worthwhile, but after Peter’s reform, education had a significant role in society, slowly changing it’s value in the eyes of the general population.</p>
<p>Another reform, which had an indirect impact on how the Russian society at the time viewed education, was the establishment of the primogeniture law. Abolishing the traditional system which a aristocrat equally divided the land among his children after death. By introducing primogeniture, the subsequent children would then need to be educated so they could earn a living by serving the state, teaching, trading or engaging on other affairs. According to Peter, this would benefit the state and bring prosperity to Russia. This decision however, was not accepted by the nobles, and the law as repealed under Tsarina Anna in 1731.</p>
<h1 id="religious-education">Religious Education</h1>
<p><img src="../images/peter-prokopovich.jpg" style="float: right" width="200" /></p>
<p>After establishing the Holy Synod in the Russian Orthodox Church, Peter would then, in 1721, issue the <em>Spiritual Regulation</em> which regulated the educational system of the Church, the author of the document was Theophan Prokopovich, a religious adviser of Peter, and he asserted that “when the light of learning is extinguished there cannot be a good order in the Church.” He tried to rebuke the popular idea among the clergy at the time that secular education would necessarily, somehow, lead to heresy and explained that just as education would benefit Russia as a state, so it would benefit the Church. In effect, the Church schools would remain religious-centered, but the curriculum would be expanded, introducing, for example, military and Church histories, the study of biographies of philosophers, astronomers, rhetoricians, historians, and the Church Fathers; they would learn from them, and consequently build upon their work.</p>
<p>The <em>Spiritual Regulation</em> did not try to affect the Orthodox dogmatic teaching, it did not seek to change the establishment of the Church, it only introduced “secular uses” to the education. The Church schools took particular interest in the quality of education and on how they were implemented, by indicating the qualifications needed for a teacher, and also by strictly regulating the curriculum.</p>
<h1 id="higher-education">Higher Education</h1>
<p>Besides founding primary schools, Peter would found institutions of higher studies outside the regulation of the Church; Church schools did not go much beyond elementary level. The idea of a secular professional education was totally foreign to the Russian school system and to the Russian mindset. In 1724, inspired and advised by Gottfried Leibniz, Peter established ‘The Saint Petersburg Academy of Sciences’ (now the ‘Russian Academy of Sciences’), a state funded institution to promote the study of languages, science, and arts. An institution of unique character at the time, one that combined the functions of education of the typical university of its time with the research of a research academy. Because of the unique characteristics of Russia, he believed that Russia required a unique educational institution as well, to spread knowledge and education throughout its land.</p>
<figure>
<img src="../images/peter-academy.jpg" width="800" alt="The building of the Imperial Academy of Sciences in Saint Petersburg" /><figcaption aria-hidden="true">The building of the Imperial Academy of Sciences in Saint Petersburg</figcaption>
</figure>
<p>Debates can arise over the nature of his reign, while he often used coercive measures to implement reforms and most of his motivations were related to war and the consolidation of his power, Peter’s place within the history of Russian education remains meaningful and substantial, he changed the way Russian society regarded education as a whole, and he established the foundation of a more efficient, robust and systematic state which successfully rivaled the western powers over the course of time.</p>
<h1 id="references">References</h1>
<ul>
<li>Riasanovksy, V. Nicholas “The Image of Peter the Great in Russian History and Thought” (1985)</li>
<li>“Peter I, ‘The Social Reforms of 1699’ in Peter the Great”, ed L. Jay Oliva (Englewood Cliffs, N.J.: Prentice-Hall, Inc., 1970), 25.</li>
<li>Cracraft, James. “Revolution of Peter the Great” (2003)</li>
<li>S.F. Platonov “Peter the Great Not a Revolutionaty Innovator”</li>
<li>Sumner, B.H. “Peter’s Accomplishments and Their Historical Significance” in Peter the Great: Reformer of Revolutionary? Ed. Marc Raeff. Boston: Heath, 1963.</li>
</ul>
    </section>
</article>

        </main>

        <footer>
            <section class="footer-box">
                    <a href="../rss.xml" aria-label="RSS feed"></a>
                    Subscribe to new posts via <a href="../rss.xml">RSS</a>
            </section>
            <section class="footer-box">
                    This site is generated by
                    <a href="https://jaspervdj.be/hakyll">Hakyll</a>,
                    the math is powered by <a href="https://www.mathjax.org/">MathJax</a>.
            </section>
        </footer>
    </body>
</html>
