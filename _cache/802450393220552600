      #6<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Paulo's Personal Webpage - How to encrypt your Hard Drive</title>
        <link rel="stylesheet" href="../css/default.css" />
        <link rel="icon" href="../images/favicon.png" />
        <body link="#BB5A5A" vlink="#BB5A5A" alink="#9A7878">
        <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
        <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js">
        </script>
    </head>
    <body>
        <header>
            <div class="logo">
                <a href="../">Paulo's Personal Webpage</a>
            </div>
            <nav>
                <a href="../">Home</a>
                <a href="../contact.html">Contact</a>
                <a href="../archive.html">Archive</a>
                <a href="../writings.html">Writings</a>
                <a href="../resources.html">Resources</a>
            </nav>
        </header>

        <main role="main">
            <h1>How to encrypt your Hard Drive</h1>
            <article>
    <section class="header">
        Posted on 2021-04-09
        
    </section>
    <section class="header">
        
        Tags: <a title="All pages tagged 'Linux'." href="../tags/Linux.html">Linux</a>, <a title="All pages tagged 'Encryption'." href="../tags/Encryption.html">Encryption</a>
        
    </section>
    <section>
        <h1 id="table-of-contents">Table of Contents</h1>
<ul>
<li><a href="#why-should-i-use-encryption">Why should I use Encryption?</a></li>
<li><a href="#types-of-encryption">Types of Encryption</a></li>
<li><a href="#what-is-cryptsetup">What is Cryptsetup?</a></li>
<li><a href="#preparation">Preparation</a></li>
<li><a href="#secure-erasure-of-the-drive-optional">Secure erasure of the drive (optional)</a></li>
<li><a href="#encryption">Encryption</a></li>
</ul>
<h2 id="why-should-i-use-encryption">Why should I use Encryption?</h2>
<p>Nowadays, in a world where there are industries centered around data mining and malicious actors are willing to spend time and money to get access to data, handling your data well is becoming more and more important, specially if it’s sensitive data like emails, credit cards, passwords, personal files and personal infomation in general. Encrypting your devices can prevent unathorized people to access your data, whether they are non-trusted people, a burgler who stole your laptop or the guy from the repair shop.</p>
<h2 id="types-of-encryption">Types of Encryption</h2>
<p><a href="https://en.wikipedia.org/wiki/Data_at_rest">Data-at-rest</a> encryption stores all the files on disk in an encrypted form. The files are only avaiable when the system is running and unlocked by a trusted party. There are two different types of methods to perform data-at-rest encryption, <a href="https://en.wikipedia.org/wiki/Filesystem-level_encryption">Filesystem-level encryption</a> which works on a File system level and the Block device encryption ( a.k.a. Full disk encryption) which operates <em>below</em> the file-system layer and ensures that all the data is encrypted inside a block device, like a Hard Drive, SSD, partition or USB flash drive. All the data stored in a encrypted block device is only accessible after mounting the device properly and having the necessary password/key to access it. We are going to use the later method.</p>
<h2 id="what-is-cryptsetup">What is Cryptsetup?</h2>
<p>To perform the block device encryption, we are going to use <a href="https://en.wikipedia.org/wiki/Dm-crypt">dm-crypt</a>, Linux kernel’s <a href="https://en.wikipedia.org/wiki/Device_mapper">device-mapper</a> encryption subsystem. The management of dm-crypt is done by the <a href="https://gitlab.com/cryptsetup/cryptsetup/">cryptsetup</a> utility. We are going to perform the <a href="https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup">LUKS Design</a> since it supports multiple keys, secure and the standard among Linux systems.</p>
<h2 id="preparation">Preparation</h2>
<p>Since we are going to perform a simple full disk encryption to protect user data, we won’t encrypt the root partition, nor the <code>/boot</code> partition. If you pretend to use a separate drive for your <code>/home</code> directory, then there isn’t much to do. But if you are going to use only one drive then you must do some partitioning, make sure to create at least a root and a <code>/home</code> partition. Obviously we are going to encrypt the home partition to secure our files at <code>/home</code>.</p>
<h2 id="secure-erasure-of-the-drive-optional">Secure erasure of the drive (optional)</h2>
<p>Before encrypting a drive it’s recommended to perform a secure erasure of the disk by overwriting the entire drive with random data or with zeros. That prevents recovery of previously stored data and cryptographic attacks. To do that first we need to create a temporary encrypted container on the desired partition (in our case the home partition) or complete device.</p>
<pre class="shell"><code># cryptsetup open --type plain -d /dev/urandom /dev/&lt;block-device&gt; to_be_wiped
</code></pre>
<p>where the <code>&lt;block-device&gt;</code> is the name of your block device, it’s something like <code>sda</code>, <code>sdb</code>, <code>sdc</code> if it’s a entire drive, or <code>sda1</code>, <code>sda2</code>, <code>sda3</code> , if it’s a partition. You can verify if the partition/device by typing <code># lsblk</code>. A container inside the partition/device with the name <code>to_be_wiped</code> will show up. To wipe the container with zeros, just type:</p>
<pre class="shell"><code># dd if=/dev/zero of=/dev/mapper/to_be_wiped status=progress
</code></pre>
<p>You could use <code>if=/dev/urandom</code> instead, but it’s not necessary. The process can take some time to complete, it depends on the size of the partition/device, if you are wiping 1TB or more of disk space it could take a couple of hours. After the process is completed you will get the following output: <code>dd: writing to ‘/dev/mapper/to_be_wiped’: No space left on device</code>.</p>
<h2 id="encryption">Encryption</h2>
<p>Now to create a LUKS header, you must type:</p>
<pre class="shell"><code># cryptsetup options luksFormat device
</code></pre>
<p>where the <code>options</code> field is entirely optional, to know more look the manpages for cryptsetup <code>man cryptsetup</code>. If you don’t know what options to use I recommend you to leave the <code>options</code> field empty, since the default options are good enough. Replace the <code>device</code> field with the previously wiped partition. After choosing a password for your device, you can accesses it by typing:</p>
<pre class="shell"><code># cryptsetup open device name
</code></pre>
<p>The <code>name</code> field is a temporary name, so you can choose whatever you want. After unlocking the partition, it will be available under the <code>/dev/mapper/name</code> path. Now you can create a file system of your choice, if you want to use the device as a <code>/home</code> partition, type:</p>
<pre><code># mkfs.ext4 /dev/mapper/name
</code></pre>
<p>Again, the name <code>field</code> is the temporary name you gave it when you opened the device/partition. If you are encrypting a USB flash drive, you might want to use <code>mkfs.vfat</code> instead. Now you can mount the device to <code>/home</code>. If you are encrypting a USB flash drive you can just close the device. To close the device, you should unmount it and:</p>
<pre class="shell"><code># cryptsetup close name
</code></pre>
<p>If you are encrypting a USB flash drive, there’s nothing left to do, you can just enjoy your newly encrypted device. But if you are encrypting your <code>/home</code> partition you might want to mount it at boot time. To do that you must add the UUID of the device to <code>/etc/crypttab</code>, to check the UUID:</p>
<pre class="shell"><code># lsblk -f
</code></pre>
<p>Add it to <code>/etc/crypttab</code> in the form:</p>
<pre class="shell"><code>name         UUID=&lt;UUID-number&gt;        none    luks,timeout=180
</code></pre>
<p>The <code>timeout</code> option defines a timeout in seconds for entering the decrypting password during boot.</p>
<p>We are done!</p>
    </section>
</article>

        </main>

        <footer>
            <section class="footer-box">
                    <a href="../rss.xml" aria-label="RSS feed"></a>
                    Subscribe to new posts via <a href="../rss.xml">RSS</a>
            </section>
            <section class="footer-box">
                    This site is generated by
                    <a href="https://jaspervdj.be/hakyll">Hakyll</a>,
                    the math is powered by <a href="https://www.mathjax.org/">MathJax</a>.
            </section>
        </footer>
    </body>
</html>
