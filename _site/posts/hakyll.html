<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Paulo's Personal Webpage - Setting up a static website with Hakyll</title>
        <link rel="stylesheet" href="../css/default.css" />
        <link rel="icon" href="../images/favicon.png" />
        <body link="#BB5A5A" vlink="#BB5A5A" alink="#9A7878">
        <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
        <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js">
        </script>
    </head>
    <body>
        <header>
            <div class="logo">
                <a href="../">Paulo's Personal Webpage</a>
            </div>
            <nav>
                <a href="../">Home</a>
                <a href="../contact.html">Contact</a>
                <a href="../archive.html">Archive</a>
                <a href="../writings.html">Writings</a>
                <a href="../resources.html">Resources</a>
            </nav>
        </header>

        <main role="main">
            <h1>Setting up a static website with Hakyll</h1>
            <article>
    <section class="header">
        Posted on 2021-04-07
        
    </section>
    <section class="header">
        
        Tags: <a title="All pages tagged 'Haskell'." href="../tags/Haskell.html">Haskell</a>, <a title="All pages tagged 'Web'." href="../tags/Web.html">Web</a>
        
    </section>
    <section>
        <h1 id="table-of-contents">Table of Contents</h1>
<ul>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#installation">Installation</a></li>
<li><a href="#building-the-website">Building the website</a></li>
<li><a href="#configuration">Configuration</a></li>
<li><a href="#rss-feed">RSS feed</a></li>
<li><a href="#adding-tags">Adding Tags</a></li>
<li><a href="#resources">Resources</a></li>
</ul>
<h2 id="introduction">Introduction</h2>
<p>This tutorial is will help you set up a <a href="https://jaspervdj.be/hakyll/">Hakyll</a> blog similar to mine. The reason I’m writing this is to have a simple, linear, step-by-step tutorial with all the things I find essential on a blog, namely a RSS feed and tags.</p>
<p>One of the advantages of using Hakyll to statically generate websites is its native integration with <a href="https://pandoc.org/">Pandoc</a>, so we can create files in any format supported by Pandoc and they will be automatically converted to HTML. In my case, for an example, I write documents using <span class="spurious-link" target="orgmode.org"><em>org-mode</em></span> inside Emacs, then I just let Hakyll do the rest of the work converting files and adding tags. It’s so simple and enjoyable to do.</p>
<p>Enough talk, let’s begin.</p>
<h2 id="installation">Installation</h2>
<p>You can install Hakyll using two different methods, the one I chose was using cabal. The other one is by using stack. Both methods are similar, so there isn’t much difference between each other. You can install cabal using <a href="https://www.haskell.org/ghcup/">ghcup</a>, or using your Linux distro standard repositories. After installing cabal on your system, you can install Hakyll by typing:</p>
<div class="sourceCode" id="cb1" data-org-language="sh"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> cabal new-install hakyll</span></code></pre></div>
<h2 id="building-the-website">Building the website</h2>
<p>You can create the default website by typing:</p>
<div class="sourceCode" id="cb2" data-org-language="sh"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> hakyll-init site-name</span></code></pre></div>
<p>where <code>site-name</code> is the name you will give to the site’s directory. Make sure that <code class="verbatim">~/.ghcup/env</code> is in your <code>$PATH</code>. Inside the <code>site-name</code> directory, you will find the default website files, execute <code>cabal new-run site</code> to set up the default website. You can check it out buy running <code>cabal new-run site watch</code>, then it will be available on your local host. Essentially, the generated website is located in the <code>_site</code> subdirectory, all the other files, except the <code>site.hs</code> file, are files <em>to be</em> converted to HTML or Templates. After making changes to some file, or adding new files to the default website, you can use <code>cabal new-run site build</code> to incrementally build your website. Although, if you make changes to <code>site.hs</code>, you need to run <code>cabal new-run site rebuild</code> instead. The <code>site.hs</code> file is where all the magic happens, it seems frightening at first, by it’s actually simple to work with.</p>
<p>You can now create files in any format supported by Pandoc and, after running <code>cabal new-run site build</code>, Hakyll will create the corresponding HTML files inside the <code>_site</code> subdirectory. You might have noticed that all files that already exist start with a block like this one:</p>
<pre><code>---
title: Something
author: Some Name
---
</code></pre>
<p>This is the Metadata of the file, and it will be useful to automatically generate Names, Dates and Tags. For example, the Metadata of this post that you are reading right now is:</p>
<pre><code>---
title: Setting up a static website with Hakyll
description: Building a Hakyll website with tags and a RSS feed
tags: Haskell, Computers
---
</code></pre>
<h2 id="configuration">Configuration</h2>
<p>We can now start to configure the website. Configuration is done by editing the <code>site.hs</code> file. Inside the file you will find some imported modules, <code>Hakyll</code> and <code>Data.Monoid (mapped)</code>, and you see the <code>hakyll</code> function being called.</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ot">main ::</span> <span class="dt">IO</span> ()</span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a>main <span class="ot">=</span> hakyll <span class="op">$</span> <span class="kw">do</span></span></code></pre></div>
<p>After that you will see some Hakyll <code>Rules</code> like these:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a>match <span class="st">&quot;images/*&quot;</span> <span class="op">$</span> <span class="kw">do</span></span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a>    route   idRoute</span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true" tabindex="-1"></a>    compile copyFileCompiler</span></code></pre></div>
<p>Here, <code>match</code> will look for all the files inside the <code>images/</code> directory and use the <code>route</code> and <code>compile</code> functions with the <code>idRoute</code> and <code>copyFileCompiler</code> arguments respectively.</p>
<p>The <code>route</code> function is used to determine the output file inside <code>_site</code>. For example, if you have a markdown file named <code>article.md</code> inside the <code>posts/</code> directory, then you will probably want Hakyll to place the converted HTML file <code>_site/posts/article.html</code> directory. That can be accomplished by using the <code>setExtension "html"</code> route. But if you don’t want to convert the file? For example, you have an image inside your <code>images/</code> directory and you just want to have a copy of that image inside the <code>_site/image/</code>, how do you do it? You just need to use the identity route <code>idRoute</code> and it’s done.</p>
<p>The compile function determines the way you want your files to be compiled. The route function just determines the place and name for the output, but to convert the file you want to compile it with something. There are different ways to do it, but if you want to Pandoc to convert your files to HTML, you should use the <code>pandocCompiler</code> argument. If you want to give Pandoc more options, then you should use <code>pandocCompilerWith</code> instead. But if you want to do nothing to the file, then just use <code>copyFileCompiler</code>.</p>
<p>Notice that there are other functions and arguments inside <code>site.hs</code> that I haven’t mentioned, you can learn more by reading the documentation for them. I just wanted to explain the ones that are most commonly used.</p>
<p>Another Rule worth mentioning is <code>create</code>, while <code>match</code> looks for a file that already exists, <code>create</code> will generate a file for you, you can find an example inside the <code>site.hs</code> file:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a>create [<span class="st">&quot;archive.html&quot;</span>] <span class="op">$</span> <span class="kw">do</span></span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true" tabindex="-1"></a>    route idRoute</span>
<span id="cb7-3"><a href="#cb7-3" aria-hidden="true" tabindex="-1"></a>    compile <span class="op">$</span> <span class="kw">do</span></span></code></pre></div>
<h2 id="rss-feed">RSS feed</h2>
<p>Setting up a RSS feed using Hakyll is easy, since Hakyll has a built-in support for it. First you will need to define a <code>FeedConfiguration</code> as the following example:</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a><span class="ot">myFeedConfiguration ::</span> <span class="dt">FeedConfiguration</span></span>
<span id="cb8-2"><a href="#cb8-2" aria-hidden="true" tabindex="-1"></a>myFeedConfiguration <span class="ot">=</span> <span class="dt">FeedConfiguration</span></span>
<span id="cb8-3"><a href="#cb8-3" aria-hidden="true" tabindex="-1"></a>    { feedTitle       <span class="ot">=</span> <span class="st">&quot;My Blog feed&quot;</span></span>
<span id="cb8-4"><a href="#cb8-4" aria-hidden="true" tabindex="-1"></a>    , feedDescription <span class="ot">=</span> <span class="st">&quot;This is the RSS feed for my blog&quot;</span></span>
<span id="cb8-5"><a href="#cb8-5" aria-hidden="true" tabindex="-1"></a>    , feedAuthorName  <span class="ot">=</span> <span class="st">&quot;John Doe&quot;</span></span>
<span id="cb8-6"><a href="#cb8-6" aria-hidden="true" tabindex="-1"></a>    , feedAuthorEmail <span class="ot">=</span> <span class="st">&quot;john@mail.com&quot;</span></span>
<span id="cb8-7"><a href="#cb8-7" aria-hidden="true" tabindex="-1"></a>    , feedRoot        <span class="ot">=</span> <span class="st">&quot;https://mywebsite.com&quot;</span></span>
<span id="cb8-8"><a href="#cb8-8" aria-hidden="true" tabindex="-1"></a>    }</span></code></pre></div>
<p>You don’t need to use all the fields above. Now you probably want to the feed to contain the content inside your posts, to do that you will need to use Snapshots to save the content inside your posts during compilation and to load then to your feed later. To do that you will need to add</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="op">&gt;&gt;=</span> saveSnapshot <span class="st">&quot;content&quot;</span></span></code></pre></div>
<p>To the <code>match "posts/*"</code> section. It will look something like this:</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true" tabindex="-1"></a>match <span class="st">&quot;posts/*&quot;</span> <span class="op">$</span> <span class="kw">do</span></span>
<span id="cb10-2"><a href="#cb10-2" aria-hidden="true" tabindex="-1"></a>    route <span class="op">$</span> setExtension <span class="st">&quot;html&quot;</span></span>
<span id="cb10-3"><a href="#cb10-3" aria-hidden="true" tabindex="-1"></a>    compile <span class="op">$</span> pandocCompiler</span>
<span id="cb10-4"><a href="#cb10-4" aria-hidden="true" tabindex="-1"></a>        <span class="op">&gt;&gt;=</span> loadAndApplyTemplate <span class="st">&quot;templates/post.html&quot;</span>    postCtx</span>
<span id="cb10-5"><a href="#cb10-5" aria-hidden="true" tabindex="-1"></a>        <span class="op">&gt;&gt;=</span> saveSnapshot <span class="st">&quot;content&quot;</span></span>
<span id="cb10-6"><a href="#cb10-6" aria-hidden="true" tabindex="-1"></a>        <span class="op">&gt;&gt;=</span> loadAndApplyTemplate <span class="st">&quot;templates/default.html&quot;</span> postCtx</span>
<span id="cb10-7"><a href="#cb10-7" aria-hidden="true" tabindex="-1"></a>        <span class="op">&gt;&gt;=</span> relativizeUrls</span></code></pre></div>
<p>Now you just need to load the content saved by the Snapshot and to create the RSS feed, to do that you will need to add the following block inside site.hs:</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true" tabindex="-1"></a>create [<span class="st">&quot;rss.xml&quot;</span>] <span class="op">$</span> <span class="kw">do</span></span>
<span id="cb11-2"><a href="#cb11-2" aria-hidden="true" tabindex="-1"></a>    route idRoute</span>
<span id="cb11-3"><a href="#cb11-3" aria-hidden="true" tabindex="-1"></a>    compile <span class="op">$</span> <span class="kw">do</span></span>
<span id="cb11-4"><a href="#cb11-4" aria-hidden="true" tabindex="-1"></a>        <span class="kw">let</span> feedCtx <span class="ot">=</span> postCtx <span class="ot">`mappend`</span> bodyField <span class="st">&quot;description&quot;</span></span>
<span id="cb11-5"><a href="#cb11-5" aria-hidden="true" tabindex="-1"></a>        posts <span class="ot">&lt;-</span> <span class="fu">fmap</span> (<span class="fu">take</span> <span class="dv">15</span>) <span class="op">.</span> recentFirst <span class="op">=&lt;&lt;</span></span>
<span id="cb11-6"><a href="#cb11-6" aria-hidden="true" tabindex="-1"></a>            loadAllSnapshots <span class="st">&quot;posts/*&quot;</span> <span class="st">&quot;content&quot;</span></span>
<span id="cb11-7"><a href="#cb11-7" aria-hidden="true" tabindex="-1"></a>        renderRss myFeedConfiguration feedCtx posts</span></code></pre></div>
<h2 id="adding-tags">Adding Tags</h2>
<p>As mentioned above, this file has the following metadata:</p>
<pre><code>---
title: Setting up a static website with Hakyll
description: Building a Hakyll website with tags and a RSS feed
tags: Haskell, Computers
---
</code></pre>
<p>You can see that I added the <code>Haskell</code> and <code>Computer</code> tags to it. To create tags we need first to fetch them inside our posts. To do that we need to add the following line to <code>site.hs</code>:</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb13-1"><a href="#cb13-1" aria-hidden="true" tabindex="-1"></a>tags <span class="ot">&lt;-</span> buildTags <span class="st">&quot;posts/*&quot;</span> fromCapture <span class="st">&quot;tags/*.html&quot;</span></span></code></pre></div>
<p>The line above will create the tags under <code>"tags/*.html"</code> format by fetching them inside the <code>"posts/*"</code> directory. Now all we need to do is to add those tags to the their respective post and to list all the tags in the archive page. To add the tags to their posts we need to change the post Context (I know I haven’t mentioned what a Context is, but we don’t need to understand them to add tags to our posts). To do that, need to remove the current <code>postCtx</code>:</p>
<div class="sourceCode" id="cb14"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb14-1"><a href="#cb14-1" aria-hidden="true" tabindex="-1"></a><span class="ot">postCtx ::</span> <span class="dt">Context</span> <span class="dt">String</span></span>
<span id="cb14-2"><a href="#cb14-2" aria-hidden="true" tabindex="-1"></a>postCtx <span class="ot">=</span></span>
<span id="cb14-3"><a href="#cb14-3" aria-hidden="true" tabindex="-1"></a>    dateField <span class="st">&quot;date&quot;</span> <span class="st">&quot;%B %e, %Y&quot;</span> <span class="ot">`mappend`</span></span>
<span id="cb14-4"><a href="#cb14-4" aria-hidden="true" tabindex="-1"></a>    defaultContext</span></code></pre></div>
<p>and add the following block in its place <code>site.hs</code>:</p>
<div class="sourceCode" id="cb15"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb15-1"><a href="#cb15-1" aria-hidden="true" tabindex="-1"></a><span class="ot">postCtxWithTags ::</span> <span class="dt">Tags</span> <span class="ot">-&gt;</span> <span class="dt">Context</span> <span class="dt">String</span></span>
<span id="cb15-2"><a href="#cb15-2" aria-hidden="true" tabindex="-1"></a>postCtxWithTags tags <span class="ot">=</span> <span class="fu">mconcat</span></span>
<span id="cb15-3"><a href="#cb15-3" aria-hidden="true" tabindex="-1"></a>    [ dateField <span class="st">&quot;date&quot;</span> <span class="st">&quot;%Y-%m-%d&quot;</span></span>
<span id="cb15-4"><a href="#cb15-4" aria-hidden="true" tabindex="-1"></a>    , tagsField <span class="st">&quot;tags&quot;</span> tags</span>
<span id="cb15-5"><a href="#cb15-5" aria-hidden="true" tabindex="-1"></a>    , defaultContext</span></code></pre></div>
<p>Then change all the occurrences of <code>postsCtx</code> inside <code>site.hs</code> to <code>postsCtxWithTags</code>. Now to add the list of tags to the <code>archive.html</code> file, we need to add the following line to the <code>create ["archive.html"]</code> rule:</p>
<div class="sourceCode" id="cb16"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb16-1"><a href="#cb16-1" aria-hidden="true" tabindex="-1"></a>field <span class="st">&quot;taglist&quot;</span> (\_ <span class="ot">-&gt;</span> renderTagList tags) <span class="ot">`mappend`</span></span></code></pre></div>
<p>In the end, it should look to something like this:</p>
<div class="sourceCode" id="cb17"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb17-1"><a href="#cb17-1" aria-hidden="true" tabindex="-1"></a>create [<span class="st">&quot;archive.html&quot;</span>] <span class="op">$</span> <span class="kw">do</span></span>
<span id="cb17-2"><a href="#cb17-2" aria-hidden="true" tabindex="-1"></a>    route idRoute</span>
<span id="cb17-3"><a href="#cb17-3" aria-hidden="true" tabindex="-1"></a>    compile <span class="op">$</span> <span class="kw">do</span></span>
<span id="cb17-4"><a href="#cb17-4" aria-hidden="true" tabindex="-1"></a>        posts <span class="ot">&lt;-</span> recentFirst <span class="op">=&lt;&lt;</span> loadAll <span class="st">&quot;posts/*&quot;</span></span>
<span id="cb17-5"><a href="#cb17-5" aria-hidden="true" tabindex="-1"></a>        <span class="kw">let</span> archiveCtx <span class="ot">=</span></span>
<span id="cb17-6"><a href="#cb17-6" aria-hidden="true" tabindex="-1"></a>                listField <span class="st">&quot;posts&quot;</span> (postCtxWithTags tags) (<span class="fu">return</span> posts) <span class="ot">`mappend`</span></span>
<span id="cb17-7"><a href="#cb17-7" aria-hidden="true" tabindex="-1"></a>                field <span class="st">&quot;taglist&quot;</span> (\_ <span class="ot">-&gt;</span> renderTagList tags) <span class="ot">`mappend`</span></span>
<span id="cb17-8"><a href="#cb17-8" aria-hidden="true" tabindex="-1"></a>                constField <span class="st">&quot;title&quot;</span> <span class="st">&quot;Archives&quot;</span>            <span class="ot">`mappend`</span></span>
<span id="cb17-9"><a href="#cb17-9" aria-hidden="true" tabindex="-1"></a>                defaultContext</span></code></pre></div>
<p>Now add this rule anywhere after that:</p>
<div class="sourceCode" id="cb18"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb18-1"><a href="#cb18-1" aria-hidden="true" tabindex="-1"></a>tagsRules tags <span class="op">$</span> \tag <span class="kw">pattern</span> <span class="ot">-&gt;</span> <span class="kw">do</span></span>
<span id="cb18-2"><a href="#cb18-2" aria-hidden="true" tabindex="-1"></a>    <span class="kw">let</span> title <span class="ot">=</span> <span class="st">&quot;Posts tagged \&quot;&quot;</span> <span class="op">++</span> tag <span class="op">++</span> <span class="st">&quot;\&quot;&quot;</span></span>
<span id="cb18-3"><a href="#cb18-3" aria-hidden="true" tabindex="-1"></a>    route idRoute</span>
<span id="cb18-4"><a href="#cb18-4" aria-hidden="true" tabindex="-1"></a>    compile <span class="op">$</span> <span class="kw">do</span></span>
<span id="cb18-5"><a href="#cb18-5" aria-hidden="true" tabindex="-1"></a>        posts <span class="ot">&lt;-</span> recentFirst <span class="op">=&lt;&lt;</span> loadAll <span class="kw">pattern</span></span>
<span id="cb18-6"><a href="#cb18-6" aria-hidden="true" tabindex="-1"></a>        <span class="kw">let</span> ctx <span class="ot">=</span> constField <span class="st">&quot;title&quot;</span> title</span>
<span id="cb18-7"><a href="#cb18-7" aria-hidden="true" tabindex="-1"></a>                  <span class="ot">`mappend`</span> listField <span class="st">&quot;posts&quot;</span> (postCtxWithTags tags) (<span class="fu">return</span> posts)</span>
<span id="cb18-8"><a href="#cb18-8" aria-hidden="true" tabindex="-1"></a>                  <span class="ot">`mappend`</span> defaultContext</span>
<span id="cb18-9"><a href="#cb18-9" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb18-10"><a href="#cb18-10" aria-hidden="true" tabindex="-1"></a>        makeItem <span class="st">&quot;&quot;</span></span>
<span id="cb18-11"><a href="#cb18-11" aria-hidden="true" tabindex="-1"></a>            <span class="op">&gt;&gt;=</span> loadAndApplyTemplate <span class="st">&quot;templates/tag.html&quot;</span> ctx</span>
<span id="cb18-12"><a href="#cb18-12" aria-hidden="true" tabindex="-1"></a>            <span class="op">&gt;&gt;=</span> loadAndApplyTemplate <span class="st">&quot;templates/default.html&quot;</span> ctx</span>
<span id="cb18-13"><a href="#cb18-13" aria-hidden="true" tabindex="-1"></a>            <span class="op">&gt;&gt;=</span> relativizeUrls</span></code></pre></div>
<p>Great, now we just need to make our tags visible. To do that we need to edit the Templates inside the <code>templates/</code> directory. Your <code>templates/posts.html</code> should look like this"</p>
<div class="sourceCode" id="cb19"><pre class="sourceCode css"><code class="sourceCode css"><span id="cb19-1"><a href="#cb19-1" aria-hidden="true" tabindex="-1"></a>&lt;article<span class="op">&gt;</span></span>
<span id="cb19-2"><a href="#cb19-2" aria-hidden="true" tabindex="-1"></a>    &lt;section class=&quot;header&quot;<span class="op">&gt;</span></span>
<span id="cb19-3"><a href="#cb19-3" aria-hidden="true" tabindex="-1"></a>        Posted on $date$</span>
<span id="cb19-4"><a href="#cb19-4" aria-hidden="true" tabindex="-1"></a>        $if(author)$</span>
<span id="cb19-5"><a href="#cb19-5" aria-hidden="true" tabindex="-1"></a>            by $author$</span>
<span id="cb19-6"><a href="#cb19-6" aria-hidden="true" tabindex="-1"></a>        $endif$</span>
<span id="cb19-7"><a href="#cb19-7" aria-hidden="true" tabindex="-1"></a>    &lt;/section<span class="op">&gt;</span></span>
<span id="cb19-8"><a href="#cb19-8" aria-hidden="true" tabindex="-1"></a>    &lt;section class=&quot;header&quot;<span class="op">&gt;</span></span>
<span id="cb19-9"><a href="#cb19-9" aria-hidden="true" tabindex="-1"></a>        $if(tags)$</span>
<span id="cb19-10"><a href="#cb19-10" aria-hidden="true" tabindex="-1"></a>        Tags<span class="in">: $tags</span>$</span>
<span id="cb19-11"><a href="#cb19-11" aria-hidden="true" tabindex="-1"></a>        $endif$</span>
<span id="cb19-12"><a href="#cb19-12" aria-hidden="true" tabindex="-1"></a>    &lt;/section<span class="op">&gt;</span></span>
<span id="cb19-13"><a href="#cb19-13" aria-hidden="true" tabindex="-1"></a>    &lt;section<span class="op">&gt;</span></span>
<span id="cb19-14"><a href="#cb19-14" aria-hidden="true" tabindex="-1"></a>        $body$</span>
<span id="cb19-15"><a href="#cb19-15" aria-hidden="true" tabindex="-1"></a>    &lt;/section<span class="op">&gt;</span></span>
<span id="cb19-16"><a href="#cb19-16" aria-hidden="true" tabindex="-1"></a>&lt;/article<span class="op">&gt;</span></span></code></pre></div>
<p>We added the tags to their respective posts. Now we’ll need to create a tag template to list the posts inside a tag, you can just copy the code inside <code>template/post-list.html</code> to a new file: <code>template/tag.html</code>. This what it should look like:</p>
<div class="sourceCode" id="cb20"><pre class="sourceCode css"><code class="sourceCode css"><span id="cb20-1"><a href="#cb20-1" aria-hidden="true" tabindex="-1"></a>$partial(&quot;templates/post-list<span class="fu">.html</span>&quot;)$</span></code></pre></div>
<p>Finally, change your <code>templates/archive.html</code> file to something like this:</p>
<div class="sourceCode" id="cb21"><pre class="sourceCode css"><code class="sourceCode css"><span id="cb21-1"><a href="#cb21-1" aria-hidden="true" tabindex="-1"></a>&lt;section<span class="op">&gt;</span></span>
<span id="cb21-2"><a href="#cb21-2" aria-hidden="true" tabindex="-1"></a>        &lt;b<span class="op">&gt;</span>Tags<span class="in">:&lt;/b</span><span class="op">&gt;</span> $taglist$</span>
<span id="cb21-3"><a href="#cb21-3" aria-hidden="true" tabindex="-1"></a>&lt;/section<span class="op">&gt;</span></span>
<span id="cb21-4"><a href="#cb21-4" aria-hidden="true" tabindex="-1"></a>&lt;br<span class="op">&gt;</span>Here is a list of all my previous posts<span class="in">:</span></span>
<span id="cb21-5"><a href="#cb21-5" aria-hidden="true" tabindex="-1"></a>$partial(&quot;templates/post-list<span class="fu">.html</span>&quot;)$</span></code></pre></div>
<p>Now you just need to rebuild <code>site.hs</code> (~cabal new-run site rebuild) and it’s done!</p>
<h2 id="resources">Resources</h2>
<p>To learn more about Hakyll, you can read these <a href="https://jaspervdj.be/hakyll/tutorials.html">tutorials</a>, and the <a href="https://hackage.haskell.org/package/hakyll">Hackage page</a>.</p>
    </section>
</article>

        </main>

        <footer>
            <section class="footer-box">
                    <a href="../rss.xml" aria-label="RSS feed"></a>
                    Subscribe to new posts via <a href="../rss.xml">RSS</a>
            </section>
            <section class="footer-box">
                    This site is generated by
                    <a href="https://jaspervdj.be/hakyll">Hakyll</a>,
                    the math is powered by <a href="https://www.mathjax.org/">MathJax</a>.
            </section>
        </footer>
    </body>
</html>
